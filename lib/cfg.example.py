# Logging Configurations

LOG_DIR = "/home/user/log/"
THREAT_LOG = "threats.log"

# Pastebin Configurations

PB_KEY = ""  # developer api key, required. GET: http://pastebin.com/api
PB_API = "https://pastebin.com/api/api_post.php"
PB_THREATS = ['test@example.com', "123 some address"]


# HIBP & Hacked-Emails Configurations

HIBP_API = "https://haveibeenpwned.com/api/v2/breachedaccount/"
HE_API = "https://hacked-emails.com/api?q="
EMAILS = ['test@example.com', "freeman@hotmail.com"]

