import cfg

def saveXml(xml, file):
    """
    saveXml
    
    :param xml: 
    :param file: 
    :return: 
    
    Used to save XML for review
    """
    # writing to file for later review if needed
    f = open(cfg.LOG_DIR + "/" + file, 'w')
    f.write("<data>")
    f.write(xml.decode())
    f.write("</data>")
    f.close()


def saveJson(js, file):
    """
    saveJson
    
    :param js: 
    :param file: 
    :return: 
    
    Used to save JSON for review
    """
    f = open(cfg.LOG_DIR + "/" + file, 'w')
    f.write("[")
    f.write(js.decode())
    f.write("]")
    f.close()

    return True


def logError(err):
    """
    logError
    
    :param err: 
    :return: 
    
    Used to log messages globally between scripts
    """
    f = open(cfg.LOG_DIR + "/error.log", 'a+')
    f.write(str(err))
    f.close()

    return True

