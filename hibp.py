#!/usr/bin/python3.5


__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft 2017 Cyb3rnetic'
__license__ = 'Open Source'

"""
HIBP Threat Intelligence

This application is used specifically for monitoring data breaches identified and
indexed by HaveIBeenPwned.com. You can configure the cfg.py EMAILS list to contain
any email address you wish to monitor.

Run this script from cron daily

"""


from urllib.error import URLError, HTTPError
from pprint import pprint

import sys

sys.path.insert(0, sys.path[0] + "/lib")

import os
import time
import urllib.parse
import urllib.request
import xml.etree.ElementTree as ET
import socket
import json
import cfg
import tilib


def main():
    """
    main

    :exit: 
    """
    for email in cfg.EMAILS:
        raw = get_breaches(email)
        if raw:
            jsdata = json.loads(raw)
            for set in jsdata:
                title = set['Title']
                date = set['BreachDate']
                added = set['AddedDate']
                if os.path.isfile(cfg.LOG_DIR + cfg.THREAT_LOG):
                    l = open(cfg.LOG_DIR + cfg.THREAT_LOG).read()
                    if ("<ident>: Found data related to a '" + title + "' breach on " + date + " that was added to HIBP on "
                            + added + " for account " + email) not in l:
                        f = open(cfg.LOG_DIR + cfg.THREAT_LOG, 'a+')
                        f.write(time.strftime("%b %d %H:%m:%S") + " " + socket.gethostname() + " " +
                                sys.argv[0] + " [" + str(os.getpid()) + "]: <ident>: Found data related to a '" +
                                title + "' breach on " + date + " that was added to HIBP on " + added + " for account " + email + "\n")
                        f.close()
                else:
                    f = open(cfg.LOG_DIR + cfg.THREAT_LOG, 'a+')
                    f.write(time.strftime("%b %d %H:%m:%S") + " " + socket.gethostname() + " " +
                            sys.argv[0] + " [" + str(os.getpid()) + "]: <ident>: Found data related to a '" +
                            title + "' breach on " + date + " that was added to HIBP on " + added + " for account " + email + "\n")
                    f.close()
            time.sleep(10)
    exit(0)


def get_breaches(email):
    """
    get_breaches
    
    :param email: 
    :return: 
    
    Return breaches for said email account
    """
    try:
        req = urllib.request.Request(url=cfg.HIBP_API + email)
        req.add_header('User-Agent', 'threat-intel-kit')
        js = urllib.request.urlopen(req).read()

        tilib.saveJson(js, "hibp-" + email + ".json")
        return js.decode()
    except HTTPError as e:
        tilib.logError(e + "\n")
        return ""
    except URLError as e:
        tilib.logError(e + "\n")
        return ""


if __name__ == "__main__":  main()

