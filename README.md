# THREAT INTELLIGENCE KIT


This is collection of Python scripts to create a "Threat Intelligence Kit" aimed to aid users in performing threat intelligence gathering.
More so, on a personal / per user basis. Each script can be configured into the users crontab to run checks on an interval of your choosing.





# GETTING STARTED


1. mkdir ~/log/

2. cd threat-intel-kit

3. vim cfg.example.py

4. mv cfg.example.py cfg.py





## PASTEBIN THREAT INTEL

1. chmod +x pb.py

2. crontab -e

3. Check hourly. Add line:

```
    @hourly /the/loc/threat-intel-kit/pb.py > /dev/null
```




## HAVEIBEENPWNED THREAT INTEL

1. chmod +x pbti.py

2. crontab -e

3. Check daily. Add line:

```
    @daily /the/loc/threat-intel-kit/hibp.py > /dev/null
```




## HACKED-EMAILS THREAT INTEL

1. chmod +x he.py

2. crontab -e

3. Check daily. Add line:

```
    @daily /the/loc/threat-intel-kit/he.py > /dev/null
```




# PYTHON VERSION

Written in version 3.5.