#!/usr/bin/python3.5


__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft 2017 Cyb3rnetic'
__license__ = 'Open Source'


"""
Pastebin Threat Intelligence

This application is used specifically for monitoring threats
on pastebin. You can configure the cfg.py PB_THREATS list to contain
any string you wish to watch for or that you consider a threat if leaked.

The script currently only monitors 'trends' and not a live paste stream.

Run this script from cron hourly

"""


from urllib.error import URLError, HTTPError

import sys

sys.path.insert(0, sys.path[0] + "/lib")

import os
import time
import urllib.parse
import urllib.request
import xml.etree.ElementTree as ET
import socket
import cfg
import tilib


def main():
    """
    main
    
    :exit: 
    """
    paste_urls = read_trending()
    for url in paste_urls:
        content = urllib.request.urlopen(url).read().decode()

        for threat in cfg.PB_THREATS:
            if threat in content:
                if os.path.isfile(cfg.LOG_DIR + cfg.THREAT_LOG):
                    l = open(cfg.LOG_DIR + cfg.THREAT_LOG).read()
                    if ("<ident>: Found a match for '" + threat + "' @ " + url) not in l:
                        f = open(cfg.LOG_DIR + cfg.THREAT_LOG, 'a+')
                        f.write(time.strftime("%b %d %H:%m:%S") + " " + socket.gethostname() + " " +
                                sys.argv[0] + " [" + str(os.getpid()) + "]: <ident>: Found a match for '" +
                                threat + "' @ " + url + "\n")
                        f.close()
                else:
                    f = open(cfg.LOG_DIR + cfg.THREAT_LOG, 'a+')
                    f.write(time.strftime("%b %d %H:%m:%S") + " " + socket.gethostname() + " " +
                            sys.argv[0] + " [" + str(os.getpid()) + "]: <ident>: Found a match for '" +
                            threat + "' @ " + url + "\n")
                    f.close()
        time.sleep(1)
    exit(0)


def read_trending():
    """
    read_trending
    
    :return: 
    
    Read tending pastes
    """
    urls = []
    xml = get_trending()

    tilib.saveXml(xml, "pastebin.xml")

    time.sleep(1)

    # lets just read in from the file
    tree = ET.parse(cfg.LOG_DIR + "/pastebin.xml")
    root = tree.getroot()

    for child in root:
        url = child.find('paste_url').text
        urls.append(url)

    return urls


def get_trending():
    """
    get_trending
    
    :return: 
    
    Get trending pastes
    """
    pastebin_vars = dict(
        api_option='trends',
        api_dev_key=cfg.PB_KEY,
    )

    try:
        return urllib.request.urlopen(cfg.PB_API, urllib.parse.urlencode(pastebin_vars).encode('utf8')).read()
    except HTTPError as e:
        tilib.logError(e + "\n")
        return ""
    except URLError as e:
        tilib.logError(e + "\n")
        return ""

    
if __name__=="__main__":  main()

